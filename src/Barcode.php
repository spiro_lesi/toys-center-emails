<?php 

namespace Newmedia;

use Picqer\Barcode\BarcodeGeneratorPNG;

class Barcode {
	public function __construct() {
		$this->generator = new BarcodeGeneratorPNG();
	}

	public function generateBarcode($code) {
		return $this->generator->getBarcode($code, $this->generator::TYPE_CODE_128);
	}
}