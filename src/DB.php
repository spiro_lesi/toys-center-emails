<?php

namespace Newmedia;

use \Doctrine\DBAL\ParameterType;
use \Doctrine\DBAL\Configuration;
use \Doctrine\DBAL\DriverManager;

use \PhpOffice\PhpSpreadsheet\IOFactory;
use \PhpOffice\PhpSpreadsheet\Reader;
use \PhpOffice\PhpSpreadsheet\Shared\Date;

class DB {
	public function __construct() {
		$config = new Configuration();
		$connectionParams = [
			'dbname' => 'toyscenter_email',
			'user' => 'root',
			'password' => 'newmedia',
			'host' => 'localhost',
			'driver' => 'pdo_mysql',
		];
		$this->conn = DriverManager::getConnection($connectionParams, $config);
		$this->barcode = new \Newmedia\Barcode;
	}

	public function getBarcodesForEmail($email) {
		$queryBuilder = $this->conn->createQueryBuilder();
		$query = $queryBuilder
			->select('type')
			->from('customers')
			->where('email = ?')
			->setParameters([$email])
			->execute()
		;
		if(!$res = $query->fetch())	return false;
		// if(array_key_exists('type', $res) == false) return false;

		$type = $res['type'];
		$data = [];
		// Query for pre_natale
		$query = $queryBuilder
			->select('*')
			->from('vouchers')
			->where(
				$queryBuilder->expr()->andX(
					$queryBuilder->expr()->eq('status', '?'),
					$queryBuilder->expr()->eq('title', '?')
				))
			->setMaxResults($type * 2)
			->setParameters([0, 'pre_natale'])
			->execute()
		;
		$data = array_merge($data, $query->fetchAll());

		// Query for post_natale
		$query = $queryBuilder
			->select('*')
			->from('vouchers')
			->where(
				$queryBuilder->expr()->andX(
					$queryBuilder->expr()->eq('status', '?'),
					$queryBuilder->expr()->eq('title', '?')
				))
			->setMaxResults($type)
			->setParameters([0, 'post_natale'])
			->execute()
		;
		$data = array_merge($data, $query->fetchAll());
		return $this->formatBarcodes($data);
	}

	public function formatBarcodes($data) {
		foreach($data as $idx => $element) {
			$data[$idx]['start_date'] = $this->formatItalianDate($element['start_date']);
			$data[$idx]['end_date'] = $this->formatItalianDate($element['end_date']);
			// $data[$idx]['barcode'] = $this->barcode->generateBarcode($element['code']);
		}
		return $data;
	}
	
	public function formatItalianDate($str) {
		$date = date_create($str);		
		$months = ['', 'Gennaio', 'Febbraio', 'Marzo', 'Aprile', 'Maggio', 'Giugno', 'Luglio', 'Agosto', 'Settembre', 'Ottobre', 'Novembre', 'Dicembre'];
		$year = $date->format('Y');				// year
		$month = $months[$date->format('n')]; 	// month number (no leading zeros)
		$day = $date->format('d');				// day
		$result = $day . ' ' . $month . ' ' . $year;
		return $result;
	}

	/*
	Function (to-do):
		importExcelData(filename, columns, additional);

	Description:
		Imports data from an excel spreadsheet and uploads into the database.

	Parameters:
		filename (string) 	- The name of the excel spreadsheet (including .xlsx extension).
		columns (array)		- A collection of all column names for the given spreadsheet.
		additional(array)	- A collection of additional columns paired with a default value.

	Returns: 
		Does not return anything.

	Example usage:
		$filename = 'excel.xlsx';
		$columns = [
			'Col1',
			'Col2',
			'ColX'
		];
		$additional = [
			'status' 	=> 0,
			'date' 		=> time()
		];
		importExcelData($filename, $columns, $additional);
	*/
	public function importExcelData($file, $title) {
		try {
			$fileType = "Xlsx";
			$fileName = "./resources/excel/$file";
			
			$reader = IOFactory::createReader($fileType);
			$reader->setReadDataOnly(true);
			$spreadsheet = $reader->load($fileName);
			$rows = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

			foreach($rows as $idx => $row) {
				$row[] = $title;
				$row[] = 0;
				$columns = $this->formatColumnArray();
				$params = $this->formatParamArray($columns, $row);
				$result = $this->insertRow($columns, $params);
				dump($result . $idx);
			}
		}
		catch(Reader\Exception $e) {
			die('Error loading file: '. $e->getMessage());
		}
	}

	public function formatColumnArray() {
		$keys = ['code', 'type', 'value', 'start_date', 'end_date', 'unknown', 'title', 'status'];
		$vals = [':p1', ':p2', ':p3', ':p4', ':p5', ':p6', ':p7', ':p8'];
		return array_combine($keys, $vals);
	}

	public function formatParamArray($cols, $row) {
		$data = array_combine(array_values($cols), array_values($row));
		$data[$cols['start_date']] = Date::excelToDateTimeObject($data[$cols['start_date']])->format('Y-m-d');
		$data[$cols['end_date']] = Date::excelToDateTimeObject($data[$cols['end_date']])->format('Y-m-d');
		return $data;
	}
	
	public function insertRow($cols, $params) {
		$queryBuilder = $this->conn->createQueryBuilder();
		$res = $queryBuilder
			->insert('vouchers')
			->values($cols)
			->setParameters($params)
			->execute()
		;
		return $res ? 'Success... Rows processed: ' : 'Failed... Rows processed: ';
	}

	public function getCodes() {
		$queryBuilder = $this->conn->createQueryBuilder();
		$res = $queryBuilder
			->select('code')
			->from('vouchers')
			->execute()
		;
		return $res->fetchAll();
	}
}