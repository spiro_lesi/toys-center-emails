<?php

namespace Newmedia;

class Mail {
	const MANDRILL_API_KEY 	= 'xn0sQs2MhKJYGvjtgykSHw';
	const SENDER_EMAIL 		= 'giocolino@toyscenter-it.com';
	const SENDER_NAME 		= 'Toys Center';
	const REPLY_TO_EMAIL 	= 'giocolino@toyscenter.it';

	public function __construct () {
		
	}

	public static function sendEmail( $mail_subject, $mail_text, $to ){
		try {
		    $mandrill = new \Mandrill( self::MANDRILL_API_KEY );
		    $message = array(
		        'html' 			=> $mail_text,
		        'subject' 		=> $mail_subject,
		        'from_email' 	=> self::SENDER_EMAIL,
		        'from_name'		=> self::SENDER_NAME,
		        'to' 			=> array( $to ),
		        'headers' 		=> array( 'Reply-To' => self::REPLY_TO_EMAIL ),
		        'important' 	=> false,
		    );

		    $async   = false;
		    $ip_pool = false;
		    #$send_at = '2016-10-11 08:58:00';

			$result  = $mandrill->messages->send( $message, $async, $ip_pool, false);
			return $result;
			
		} catch ( Exception $e ) {
		    echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}
}