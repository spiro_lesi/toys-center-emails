<?php

require_once __DIR__ . '/vendor/autoload.php';

$mail = new \Newmedia\Mail;
$db = new \Newmedia\DB;
$barcode = new \Newmedia\Barcode;

$emails = [
	'spiro@newmedia.al'
];

$subject = 'Grazie per aver acquistato su toyscenter.it, ecco i tuoi buoni sconto! - [Type: 2]';
foreach($emails as $email) {
	$vouchers = $db->getBarcodesForEmail($email);
	// ob_start();
	include __DIR__ . '/resources/templates/index.php';
	// $template = ob_get_clean();
	// $result = $mail->sendEmail($subject, $template, ['email' => $email]);
	// dump($result);
}

// $codes = $db->getCodes();

// foreach($codes as $c) {
// 	$content = $barcode->generateBarcode($c['code']);
// 	file_put_contents(__DIR__ . '/img/' . $c['code'] . '.svg', $content);
// }