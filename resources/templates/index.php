<?php global $vouchers; ?>

<!DOCTYPE html>
<html>
	<head>
		<title>Toys Center Email</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<style>
			body {
				font-family: Arial, Helvetica, sans-serif;
			}
			#container {
				max-width: 720px;
				margin: 0 auto;
				padding: 3rem;
			}
			#table-layout {
				border-collapse: separate;
				border: 0;
				margin: 0 auto;
			}
			table, thead, tbody, tfoot, tr {
				width: 100%;
			}
			h1, h2, h4, h4, h5, h6 {
				margin-block-start: 0; 
				margin-block-end: 0.5em;
			}
			.text-primary {
				color: #333;
			}
			.text-secondary {
				color: #E1061A;
			}
			.text-center {
				text-align: center;
			}
			.caption {
				text-align: left;
				caption-side: bottom;
				color: #666;
				font-size: 14px;
			}
			.img-fluid {
				max-width: 100%;
				height: auto;
			}
			.header,
			.footer {
				font-size: 32px;
				font-weight: bold;
				text-transform: uppercase;
			}
			.content {
				font-size: 18px;
				font-weight: bold;
			}
			.voucher-title {
				text-transform: uppercase;
			}
			.voucher-title td {
				padding-bottom: 2rem;
			}
			.voucher td {
				padding-bottom: 2rem;
			}
			svg {
				
			}
			@media (max-width: 767px) {
				.header,
				.footer {
					font-size: 24px;
				}
				.content {
					font-size: 18px;
				}
			}
			@media (max-width: 480px) {
				#container {
					padding: 0.5rem;
				}
				.header,
				.footer {
					font-size: 18px;
				}
				.content {
					font-size: 14px;
				}
			}
		</style>
	</head>
	<body>
		<section id="container">
			<table id="table-layout">
				<thead class="text-secondary text-center">
					<tr class="header">
						<th>
							<img class="img-fluid" src="https://storage.googleapis.com/toyscenter-campaign/toyscenter-logo.png" alt="Toys Center Logo" />
							<h4>Un magico natale da toys center!</h4>
							<h6>E il regalo te lo facciamo noi!</h6>
						</th>
					</tr>
				</thead>
				<tbody class="text-primary text-center">
					<tr class="content">
						<td>
							<p>Grazie per aver acquistato sul sito di Toys Center!</p>
							<p>Grazie per aver acquistato sul sito di Toys Center! Per ogni acquisto effettuato dal 30 novembre al 9 dicembre 2018 ogni 60&euro; ti rimborsiamo il 100% in buoni spesa per i tuoi acquisti successivi!</p>
							<p>I buoni potranno essere utilizzati con le seguenti modalità:</p>
							<p>Ogni buono spesa del valore di 20&euro; potrà essere utilizzato a fronte di un acquisto minimo di 60&euro; con unico scontrino per l’acquisto di giocattoli e tantissimi altri articoli (vedi esclusioni*).</p>
						</td>
					</tr>
					<tr class="voucher-title">
						<td><h4>Ecco qui i tuoi buoni spesa:</h4></td>
					</tr>
				</tbody>
				<tbody class="text-primary text-center">
					<?php foreach($vouchers as $idx => $voucher): ?>
						<tr class="voucher">
							<td>
								<h4>Buono &deg;<?php echo $idx + 1; ?></h4>
								<div><img class="img-fluid" src="https://storage.googleapis.com/toyscenter-campaign/barcodes/<?php echo $voucher['code']; ?>.png" alt="Barcode" /></div>
								<p>Buono spesa valido solo in negozio a fronte di un acquisto minimo di 60€ (vedi esclusioni*) dal <?php echo $voucher['start_date']; ?> al <?php echo $voucher['end_date']; ?>.</p>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
				<tfoot class="text-secondary text-center">
					<tr>
						<th class="footer">
							<h3>Ti aspettiamo</h3>
							<h6>Nei nostri punti vendita per vivere</h6>
							<h3>La magia del natale</h3>
						</th>
					</tr>
				</tfoot>
				<caption class="caption">
					<p>* Ad esclusione di: alimentari, baby food, pannolini, latti speciali (es. latte 1 e il latte ai fini medici), libri, tessile e abbigliamento, video games, shopper, prodotti a marchio Claire's e gift card Toys Center. I buoni non sono rimborsabili, non opossono essere convertiti in denaro, non danno dirotto a resto e non sono utilizzabili con altri buoni sconto. Potranno essere erogati contestualmente allo scontrino d'acquisto un numero massimo di 9 buoni spesa (anche se l'imoprto totale acquistato risulta essere superiore a &euro;180.00)</p>
				</caption>
			</table>
		</section>
	</body>
</html>